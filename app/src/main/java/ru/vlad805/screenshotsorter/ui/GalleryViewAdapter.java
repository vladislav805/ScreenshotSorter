package ru.vlad805.screenshotsorter.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import ru.vlad805.screenshotsorter.R;
import ru.vlad805.screenshotsorter.db.Image;

import java.util.ArrayList;
import java.util.List;

public class GalleryViewAdapter extends RecyclerView.Adapter<GalleryViewAdapter.ViewHolder> {

	public static final int COLUMN_COUNT = 3;

	private List<Image> mItems;
	private DisplayMetrics mMetrics;
	private Context mContext;
	private boolean mIsActionMode = false;
	private List<Integer> mSelected;

	public GalleryViewAdapter(List<Image> items, DisplayMetrics dm) {
		mItems = items;
		mMetrics = dm;
		mSelected = new ArrayList<>();
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		mContext = parent.getContext();
		View view = LayoutInflater.from(mContext).inflate(R.layout.item_image, parent, false);

		view.setLayoutParams(new RelativeLayout.LayoutParams(mMetrics.widthPixels / COLUMN_COUNT, mMetrics.widthPixels / COLUMN_COUNT));
		return new ViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
		holder.update(mItems.get(position), position);
	}

	@Override
	public int getItemCount() {
		return null != mItems ? mItems.size() : 0;
	}

	/*
	 * Selection
	 */

	public void startActionMode() {
		mIsActionMode = true;
		notifyDataSetChanged();
	}

	public void destroyActionMode() {
		mIsActionMode = false;
		mSelected.clear();
		notifyDataSetChanged();
	}

	public void setSelectionItem(int index) {
		setSelectionItem(index, !mSelected.contains(index));
	}

	public void setSelectionItem(int index, boolean state) {
		if (state) { // добавить
			mSelected.add(index);
		} else { // удалить из выделенных
			mSelected.remove(Integer.valueOf(index));
		}

		notifyItemChanged(index);
	}

	public Image[] getSelectedItems() {
		if (!mIsActionMode) {
			return null;
		}

		Image[] data = new Image[mSelected.size()];

		for (int i = 0, l = mSelected.size(); i < l; ++i) {
			data[i] = mItems.get(mSelected.get(i));
		}

		return data;
	}

	class ViewHolder extends RecyclerView.ViewHolder {

		private View mRoot;
		private ImageView mImage;
		private CheckBox mCheckbox;

		ViewHolder(View v) {
			super(v);
			mRoot = v;
			mImage = v.findViewById(R.id.image_view);
			mCheckbox = v.findViewById(R.id.image_checkbox);
		}

		void update(Image img, int index) {
			mImage.setImageBitmap(BitmapFactory.decodeFile(img.getPath()));

			int p = img.isViewed() > 0 ? 2 : 6;
			mRoot.setPadding(p, p, p, p);
			mRoot.setBackgroundColor(img.isViewed() > 0 ? Color.TRANSPARENT : Color.RED);

			if (mIsActionMode) {
				mCheckbox.setVisibility(View.VISIBLE);

				mCheckbox.setChecked(mSelected.contains(index));
			} else {
				mCheckbox.setVisibility(View.GONE);
			}
		}
	}
}