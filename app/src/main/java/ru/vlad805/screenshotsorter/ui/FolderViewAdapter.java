package ru.vlad805.screenshotsorter.ui;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import ru.nikartm.support.ImageBadgeView;
import ru.vlad805.screenshotsorter.*;
import ru.vlad805.screenshotsorter.db.Folder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FolderViewAdapter extends RecyclerView.Adapter<FolderViewAdapter.ViewHolder> implements ItemTouchHelperAdapter {

	/**
	 * Данные о папках
	 */
	private List<Folder> mItems;

	/**
	 * Интерфейс отвечающий за перемещение папки
	 */
	private final OnStartDragListener mDragStartListener;

	/**
	 * Колбек, вызывемый при перемещении папки
	 */
	private OnReorderFolders mOnReorderFolders;

	/**
	 * Колбек для элементов при долгом зажатии
	 */
	private OnLongClickItemListener mOnLongClickListener;

	/**
	 * Флаг, определяющий, нужно ли отключать возможности перемещения, меню и счетчика
	 * (используется в активити с сохранением скрина)
	 */
	private boolean mIsLiteMode = false;

	/**
	 * Массив с изменениями положений при перемещении
	 */
	private List<Pair<Integer, Integer>> mMoves = new ArrayList<>();

	public interface OnReorderFolders {
		void onReorder(List<Pair<Integer, Integer>> diff);
	}

	public interface OnLongClickItemListener {
		void onLongClick(int position);
	}

	public FolderViewAdapter(OnStartDragListener dragStartListener) {
		mItems = new ArrayList<>();
		mDragStartListener = dragStartListener;
	}

	/**
	 * Создание новых View и ViewHolder элемента списка, которые впоследствии могут переиспользоваться.
	 */
	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
		View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_folder, viewGroup, false);
		return new ViewHolder(v);
	}

	/**
	 * Заполнение виджетов View данными из элемента списка с номером position
	 */
	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		Folder item = mItems.get(position);
		holder.update(item, position);
	}

	/**
	 * Вызывается при перемещении папок (по одной)
	 * @param fromPosition The start position of the moved item.
	 * @param toPosition   Then resolved position of the moved item.
	 * @return ничего
	 */
	@Override
	public boolean onItemMove(int fromPosition, int toPosition) {
		mMoves.add(new Pair<>(fromPosition, toPosition));

		Collections.swap(mItems, fromPosition, toPosition);
		notifyItemMoved(fromPosition, toPosition);
		return true;
	}

	/**
	 * Вызывается при отпускании папки после перемещения
	 * На этом моменте очищается лог, перед этим оповещается слушатель, где
	 * сохраняются все изменения
	 */
	@Override
	public void onItemDrop() {
		List<Pair<Integer, Integer>> cache = new ArrayList<>(mMoves);
		mOnReorderFolders.onReorder(cache);
		mMoves.clear();
	}

	/**
	 * Не используется
	 * @param position The position of the item dismissed.
	 */
	@Override
	public void onItemDismiss(int position) {

	}

	@Override
	public int getItemCount() {
		return mItems.size();
	}

	/**
	 * Смена колбека для перемещений
	 * @param listener слушатель событий
	 */
	public void setOnReorderFolders(OnReorderFolders listener) {
		mOnReorderFolders = listener;
	}

	/**
	 * Смена колбека для долгого зажатия
	 * @param listener слушатель событий
	 */
	public void setOnLongClickListener(OnLongClickItemListener listener) {
		mOnLongClickListener = listener;
	}

	/**
	 * Изменение данных о папок
	 * @param items массив с папками
	 */
	public void setSource(List<Folder> items) {
		mItems = items;
	}

	/**
	 * Смена режима адаптера: при true отключается меню (придолгом зажатии,
	 * перемещение и счетчики)
	 * @param lite состояние
	 */
	public void setLiteMode(boolean lite) {
		mIsLiteMode = lite;
	}

	/**
	 * Реализация класса ViewHolder, хранящего ссылки на виджеты.
	 */
	class ViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder, View.OnTouchListener, View.OnLongClickListener {

		private ImageBadgeView mIcon;
		private TextView mName;
		private ImageView mHandle;
		private int mPosition;

		@SuppressLint("ClickableViewAccessibility")
		ViewHolder(View v) {
			super(v);
			mIcon = v.findViewById(R.id.folder_item_icon);
			mName = v.findViewById(R.id.folder_item_title);
			mHandle = v.findViewById(R.id.folder_item_handle);

			mIcon.setShowCounter(!mIsLiteMode);

			if (!mIsLiteMode) {
				v.setOnLongClickListener(this);

				mHandle.setOnTouchListener(this);
			} else {

				mHandle.setVisibility(View.GONE);
			}
		}

		void update(Folder folder, int position) {
			mPosition = position;
			mIcon.setImageResource(getIcon(folder.isSystem()));

			mName.setText(folder.getTitle());
			if (!mIsLiteMode) {
				mIcon.setShowCounter(folder.getCount() > 0);
				if (folder.getCount() > 0) {
					mIcon.setBadgeValue(folder.getCount());
				}
			}
		}

		int getIcon(int state) {
			switch (state) {
				case Folder.SYSTEM_SEE_LATER:
					return R.drawable.ic_folder_later;

				default:
					return R.drawable.ic_folder_open;
			}
		}

		@Override
		public void onItemSelected() {
			itemView.setBackgroundColor(Color.argb(128, 128, 128, 128));
		}

		@Override
		public void onItemClear() {
			itemView.setBackgroundColor(0);
		}

		@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (mDragStartListener != null && event.getAction() == MotionEvent.ACTION_DOWN) {
				mDragStartListener.onStartDrag(this);
				return true;
			}

			return true;
		}

		@Override
		public boolean onLongClick(View view) {
			mOnLongClickListener.onLongClick(mPosition);
			return false;
		}
	}
}