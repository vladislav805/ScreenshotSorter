package ru.vlad805.screenshotsorter.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import ru.vlad805.screenshotsorter.R;

@SuppressWarnings("WeakerAccess")
public class ImageOverlayView extends RelativeLayout {

	private View mRoot;
	private Toolbar mToolbar;
	private TextView mDescription;

	private OnMenuItemClicked mMenuListener;
	private OnDescriptionEditedListener mDescriptionListener;

	public interface OnMenuItemClicked {
		void onClicked(int button);
	}

	public static final int BUTTON_DESCRIPTION = R.id.navigation_viewer_menu_text;
	public static final int BUTTON_SHARE = R.id.navigation_viewer_menu_share;
	public static final int BUTTON_EDIT = R.id.navigation_viewer_menu_edit;
	public static final int BUTTON_MORE = R.id.navigation_viewer_menu_more;
	public static final int BUTTON_DELETE = R.id.navigation_viewer_menu_delete;
	public static final int BUTTON_TOGGLE_UNVIEWED = R.id.navigation_viewer_menu_set_unviewed;

	public ImageOverlayView(Context context) {
		super(context);
		init();
	}

	public ImageOverlayView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public ImageOverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	public ImageOverlayView setTitle(String title) {
		mToolbar.setTitle(title);
		return this;
	}

	public ImageOverlayView setSubtitle(String subtitle) {
		mToolbar.setSubtitle(subtitle);
		return this;
	}

	public ImageOverlayView setDescription(String description) {
		mDescription.setText(description);
		if (description == null || description.isEmpty()) {
			mDescription.setVisibility(GONE);
		} else {
			mDescription.setVisibility(VISIBLE);
		}
		return this;
	}

	private void init() {
		mRoot = inflate(getContext(), R.layout.view_image_overlay, this);
		mToolbar = mRoot.findViewById(R.id.viewer_toolbar);
		mDescription = mRoot.findViewById(R.id.viewer_description);

		mToolbar.setNavigationIcon(R.drawable.ic_arrow_back);

		//mDescription.setMovementMethod(LinkMovementMethod.getInstance());
		mRoot.findViewById(BUTTON_MORE).setOnClickListener(this::onMoreClick);
	}

	private View.OnClickListener mViewListener = view -> mMenuListener.onClicked(view.getId());

	public void setAllClicksListener(OnMenuItemClicked l1, OnDescriptionEditedListener l2) {
		mToolbar.setNavigationOnClickListener(v -> mMenuListener.onClicked(android.R.id.home));

		int q[] = {BUTTON_SHARE, BUTTON_EDIT, BUTTON_DESCRIPTION};

		for (int i : q) {
			mRoot.findViewById(i).setOnClickListener(mViewListener);
		}

		mMenuListener = l1;
		mDescriptionListener = l2;
	}

	@SuppressWarnings("ParameterCanBeLocal")
	private void onDescriptionClick(View view) {





		view = inflate(getContext(), R.layout.dialog_description, null);
		EditText et = view.findViewById(R.id.dialog_input);
		AlertDialog.Builder ab = new AlertDialog.Builder(getContext())
				.setTitle(R.string.screen_description_title)
				.setView(view)
				.setPositiveButton(android.R.string.ok, (dialog, which) -> {
					String text = et.getText().toString().trim();
					if (mDescriptionListener != null) {
						mDescriptionListener.onDescriptionEdited(text);
					}
					setDescription(text);
				})
				.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());
		et.setText(mDescription.getText().toString());
		ab.create().show();

		et.requestFocus();
	}

	private void onMoreClick(View v) {
		PopupMenu popup = new PopupMenu(v.getContext(), v);
		// Inflate the menu from xml
		popup.inflate(R.menu.popup_viewer);
		// Setup menu item selection
		popup.setOnMenuItemClickListener(item -> {
			mMenuListener.onClicked(item.getItemId());
			return true;
		});
		popup.show();
	}

	public interface OnDescriptionEditedListener {
		void onDescriptionEdited(String text);
	}
}