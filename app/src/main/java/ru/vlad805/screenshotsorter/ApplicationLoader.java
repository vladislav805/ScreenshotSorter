package ru.vlad805.screenshotsorter;

import android.app.Application;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Handler;
import com.facebook.drawee.backends.pipeline.Fresco;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * vlad805 (c) 2018
 */
@SuppressWarnings("WeakerAccess")
public class ApplicationLoader extends Application {

	public static volatile Context applicationContext;
	public static volatile Handler applicationHandler;

	@Override
	public void onCreate() {
		super.onCreate();

		Thread.setDefaultUncaughtExceptionHandler((thread, e) -> {
			// Get the stack trace.getThumbDrawable
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);

			// Add it to the clip board and close the app
			ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
			ClipData clip = ClipData.newPlainText("Stack trace", "```" + sw.toString() + "```");
			clipboard.setPrimaryClip(clip);
			System.exit(1);
		});

		applicationContext = getApplicationContext();
		applicationHandler = new Handler(applicationContext.getMainLooper());
		Fresco.initialize(this);
	}
}
