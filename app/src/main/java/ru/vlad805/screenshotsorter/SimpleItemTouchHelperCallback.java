package ru.vlad805.screenshotsorter;

import android.graphics.Canvas;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import org.jetbrains.annotations.NotNull;

/**
 * An implementation of {@link ItemTouchHelper.Callback} that enables basic drag & drop and
 * swipe-to-dismiss. Drag events are automatically started by an item long-press.<br/>
 * </br/>
 * Expects the <code>RecyclerView.Adapter</code> to listen for {@link
 * ItemTouchHelperAdapter} callbacks and the <code>RecyclerView.ViewHolder</code> to implement
 * {@link ItemTouchHelperViewHolder}.
 *
 * @author Paul Burke (ipaulpro)
 */
public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

	private static final float ALPHA_FULL = 1.0f;

	private final ItemTouchHelperAdapter mAdapter;

	private int dragFrom = -1;
	private int dragTo = -1;

	public SimpleItemTouchHelperCallback(ItemTouchHelperAdapter adapter) {
		mAdapter = adapter;
	}

	@Override
	public boolean isLongPressDragEnabled() {
		return false;
	}

	@Override
	public boolean isItemViewSwipeEnabled() {
		return false;
	}

	@Override
	public int getMovementFlags(@NotNull RecyclerView recyclerView, @NotNull RecyclerView.ViewHolder viewHolder) {
		// Set movement flags based on the layout manager
		if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
			final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
			final int swipeFlags = 0;
			return makeMovementFlags(dragFlags, swipeFlags);
		} else {
			final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
			final int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
			return makeMovementFlags(dragFlags, swipeFlags);
		}
	}

	@Override
	public boolean onMove(@NotNull RecyclerView recyclerView, @NotNull RecyclerView.ViewHolder source, @NotNull RecyclerView.ViewHolder target) {
		if (source.getItemViewType() != target.getItemViewType()) {
			return false;
		}

		int fromPosition = source.getAdapterPosition();
		int toPosition = target.getAdapterPosition();

		if (dragFrom == -1) {
			dragFrom = fromPosition;
		}
		dragTo = toPosition;

		// Notify the adapter of the move
		mAdapter.onItemMove(fromPosition, toPosition);
		return true;
	}

	@Override
	public void onSwiped(@NotNull RecyclerView.ViewHolder viewHolder, int i) {
		// Notify the adapter of the dismissal
		mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
	}

	@Override
	public void onChildDraw(@NotNull Canvas c, @NotNull RecyclerView recyclerView, @NotNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
		if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
			// Fade out the view as it is swiped out of the parent's bounds
			final float alpha = ALPHA_FULL - Math.abs(dX) / (float) viewHolder.itemView.getWidth();
			viewHolder.itemView.setAlpha(alpha);
			viewHolder.itemView.setTranslationX(dX);
		} else {
			super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
		}
	}

	@Override
	public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
		// We only want the active item to change
		if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
			if (viewHolder instanceof ItemTouchHelperViewHolder) {
				// Let the view holder know that this item is being moved or dragged
				ItemTouchHelperViewHolder itemViewHolder = (ItemTouchHelperViewHolder) viewHolder;
				itemViewHolder.onItemSelected();
			}
		}

		super.onSelectedChanged(viewHolder, actionState);
	}

	@Override
	public void clearView(@NotNull RecyclerView recyclerView, @NotNull RecyclerView.ViewHolder viewHolder) {
		super.clearView(recyclerView, viewHolder);

		viewHolder.itemView.setAlpha(ALPHA_FULL);

		if(dragFrom != -1 && dragTo != -1 && dragFrom != dragTo) {
			mAdapter.onItemDrop();
		}

		dragFrom = dragTo = -1;

		if (viewHolder instanceof ItemTouchHelperViewHolder) {
			// Tell the view holder it's time to restore the idle state
			ItemTouchHelperViewHolder itemViewHolder = (ItemTouchHelperViewHolder) viewHolder;
			itemViewHolder.onItemClear();
		}
	}
}