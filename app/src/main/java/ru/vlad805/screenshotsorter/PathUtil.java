package ru.vlad805.screenshotsorter;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import java.util.Objects;

/**
 * Created by Aki on 1/7/2017.
 */
@SuppressWarnings("WeakerAccess")
public class PathUtil {
	/*
	 * Gets the file path of the given Uri.
	 */

	public static String getPath(Context context, Uri uri) {
		String selection = null;
		String[] selectionArgs = null;

		// Uri is different in versions after KITKAT (Android 4.4), we need to
		// deal with different Uris.
		if (DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
			if (isExternalStorageDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				return Environment.getExternalStorageDirectory() + "/" + split[1];
			} else if (isDownloadsDocument(uri)) {
				final String id = DocumentsContract.getDocumentId(uri);
				uri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
			} else if (isMediaDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];
				switch (type) {
					case "image": uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI; break;
					case "video": uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI; break;
					case "audio": uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI; break;
				}
				selection = "_id=?";
				selectionArgs = new String[]{split[1]};
			}
		}
		if ("content".equalsIgnoreCase(uri.getScheme())) {
			String[] projection = {MediaStore.Images.Media.DATA};

			try (Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null)) {
				int column_index = Objects.requireNonNull(cursor).getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				if (cursor.moveToFirst()) {
					return cursor.getString(column_index);
				}
			} catch (Exception ignore) {}
		} else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}
		return null;
	}


	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri.getAuthority());
	}
}