package ru.vlad805.screenshotsorter;

/**
 * vlad805 (c) 2018
 */
@SuppressWarnings("WeakerAccess")
public class Config {
	public static final boolean DEBUG = true;

	public static final String KEY_PATH = "path";
	public static final String DB_NAME = "db";
	public static final String KEY_FOLDER = "folderTitle";
	public static final String DIALOG_EXTRA_BUTTON = "dialog_extra_button";
	public static final String DIALOG_EXTRA_BUTTON_LABEL = "dialog_extra_button_label";
	public static final String DIALOG_RESULT = "dialog_result";
	public static final String DIALOG_DEFAULT_TEXT = "dialog_default_text";
}
