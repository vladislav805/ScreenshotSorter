package ru.vlad805.screenshotsorter;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.WindowManager;
import android.widget.Toast;
import ru.vlad805.screenshotsorter.db.AppDatabase;
import ru.vlad805.screenshotsorter.db.Image;
import ru.vlad805.screenshotsorter.service.UpdateBadgeService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;

/**
 * vlad805 (c) 2018
 */
@SuppressWarnings("WeakerAccess")
public class Utils {

	@SuppressWarnings("unused")
	private static final String TAG = Utils.class.getName();
	private Context mContext;

	public Utils(Context ctx) {
		mContext = ctx;
	}

	public static String getDateString(long date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US);
		return format.format(new Date(date));
	}

	public static Point getRealScreenSize(Context context) {
		Point size = new Point();
		try {
			WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			assert windowManager != null;
			windowManager.getDefaultDisplay().getRealSize(size);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return size;
	}

	public static boolean checkIfSizesEquals(Point p1, Point p2) {
		return Math.max(p1.x, p1.y) == Math.max(p2.x, p2.y) && Math.min(p1.x, p1.y) == Math.min(p2.x, p2.y);
	}

	@SuppressWarnings({"ConstantConditions", "deprecation"})
	public boolean isServiceRunning(Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	private Uri aviaryFix(Uri uri) {
		String uri2 = uri.toString();
		if (!uri2.startsWith("/") || !uri2.contains("AVIARY")) {
			return uri;
		}
		return Uri.parse("file://" + uri2);
	}

	public int dip2px(float dpValue) {
		return (int) ((dpValue * mContext.getResources().getDisplayMetrics().density) + 0.5f);
	}

	public int px2dip(float pxValue) {
		return (int) ((pxValue / mContext.getResources().getDisplayMetrics().density) + 0.5f);
	}

	public interface DialogResultListener {
		void onPositive();
		default void onNegative() {

		}
		default void onNeural() {

		}
	}

	public static void confirm(Context context, int title, int text, int positive, int negative, int neural, DialogResultListener listener) {
		confirm(
				context,
				title > 0 ? context.getString(title) : null,
				context.getString(text),
				context.getString(positive),
				negative > 0 ? context.getString(negative) : null,
				neural > 0 ? context.getString(neural) : null,
				listener
		);
	}

	public static void confirm(Context context, String title, String text, String positive, String negative, String neural, DialogResultListener listener) {
		final AlertDialog.Builder ab = new AlertDialog.Builder(context);

		if (title != null) {
			ab.setTitle(title);
		}

		if (text != null) {
			ab.setMessage(text);
		}

		DialogInterface.OnClickListener proxy = (dialogInterface, which) -> {
			switch (which) {
				case DialogInterface.BUTTON_POSITIVE: listener.onPositive(); break;
				case DialogInterface.BUTTON_NEGATIVE: listener.onNegative(); break;
				case DialogInterface.BUTTON_NEUTRAL: listener.onNeural(); break;
			}
		};

		if (positive != null) {
			ab.setPositiveButton(positive, proxy);
		}

		if (negative != null) {
			ab.setNegativeButton(negative, proxy);
		}

		if (neural != null) {
			ab.setNeutralButton(neural, proxy);
		}

		ab.create().show();
	}

	public interface OnImageRemoveListener {
		void onRemove(boolean alsoFile);
	}

	public static void showRemoveFileDialog(Context context, AppDatabase db, Image obj, OnImageRemoveListener callback) {
		final AlertDialog.Builder ab = new AlertDialog.Builder(context);

		ab.setTitle(context.getString(R.string.gallery_remove_confirmation_text));

		boolean isInternal = obj.getPath().contains(BuildConfig.APPLICATION_ID);

		AtomicReference<Boolean> alsoFile = new AtomicReference<>(false);

		if (!isInternal) {
			ab.setMultiChoiceItems(
					new CharSequence[] {context.getString(R.string.gallery_remove_confirmation_also_file)},
					null,
					(dialogInterface, index, isChecked) -> alsoFile.set(isChecked)
			);
		} else {
			alsoFile.set(true);
		}

		ab.setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
			if (alsoFile.get()) {
				removeFile(context, obj.getPath());
			}

			if (db != null) {
				new Thread(() -> db.getImageDAO().delete(obj)).start();
			}

			callback.onRemove(alsoFile.get());
		});


		ab.setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

		ab.create().show();
	}

	public static void showRemoveFilesDialog(Context context, AppDatabase db, Image[] obj, OnImageRemoveListener callback) {
		final AlertDialog.Builder ab = new AlertDialog.Builder(context);

		ab.setTitle(context.getString(R.string.gallery_remove_confirmation_text));

		boolean isInternal = false;

		for (Image img : obj) {
			isInternal |= img.getPath().contains(BuildConfig.APPLICATION_ID);
		}

		AtomicReference<Boolean> alsoFile = new AtomicReference<>(false);

		if (!isInternal) {
			ab.setMultiChoiceItems(
					new CharSequence[] {context.getString(R.string.gallery_remove_confirmation_also_file)},
					null,
					(dialogInterface, index, isChecked) -> alsoFile.set(isChecked)
			);
		} else {
			alsoFile.set(true);
		}

		ab.setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
			if (alsoFile.get()) {
				for (Image img : obj) {
					removeFile(context, img.getPath());
				}
			}

			if (db != null) {
				new Thread(() -> db.getImageDAO().delete(obj)).start();
			}

			callback.onRemove(alsoFile.get());
		});


		ab.setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());

		ab.create().show();
	}

	@SuppressWarnings("ResultOfMethodCallIgnored")
	public static void removeFile(Context context, String path) {
		if (context != null) {
			context.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, MediaStore.Images.ImageColumns.DATA + "=?", new String[]{path});
		}
		File f = new File(path);
		if (f.exists()) {
			f.delete();
		}
	}

	/**
	 * Копирование из файла в файл
	 * @param in поток исходного файла
	 * @param out поток нового файла
	 * @throws IOException io
	 */
	public static void copy(InputStream in, OutputStream out) throws IOException {
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
	}

	/**
	 * Открытие интента для редактирования и выбора приложения
	 * @param context контекст
	 * @param path реальный путь до файла
	 * @param requestCode код запроса для отлова его в активити после сохранения
	 */
	public static void openImageEditor(Activity context, String path, int requestCode) {
		Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID, new File(path));

		Intent editIntent = new Intent(Intent.ACTION_EDIT);

		editIntent.setDataAndType(uri, "image/*");
		editIntent.putExtra("outputFormat", Bitmap.CompressFormat.PNG);
		editIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

		int flags = Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
		editIntent.addFlags(flags);

		// Code taken from http://stackoverflow.com/questions/24835364/android-open-private-file-with-third-party-app
		List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(editIntent, PackageManager.MATCH_DEFAULT_ONLY);

		Iterator it = resInfoList.iterator();

		while (it.hasNext()) {
			ResolveInfo ri = (ResolveInfo) it.next();

			if (!ri.activityInfo.packageName.contains("google")) {
				it.remove();
			}
		}

		if (resInfoList.size() == 0) {
			Toast.makeText(context, R.string.screen_action_edit_no_app, Toast.LENGTH_LONG).show();
			return;
		}

		for (ResolveInfo resolveInfo : resInfoList) {
			context.grantUriPermission(resolveInfo.activityInfo.packageName, uri, flags);
		}

		ResolveInfo ri = resInfoList.get(0);
		editIntent.setComponent(new ComponentName(ri.activityInfo.packageName, ri.activityInfo.name));

		//if (resInfoList.size() == 1) {
			context.startActivityForResult(editIntent, requestCode);
		/*} else {
			context.startActivityForResult(Intent.createChooser(editIntent, null), requestCode);
		}*/
	}

	/**
	 * Открытие интента поделиться для выбора приложения
	 * @param ctx контекст
	 * @param path реальный путь до файла (изображения)
	 */
	public static void openShareIntent(Context ctx, String path) {
		Uri uri = FileProvider.getUriForFile(ctx, BuildConfig.APPLICATION_ID, new File(path));
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setData(uri);
		sharingIntent.putExtra(Intent.EXTRA_STREAM, uri);
		sharingIntent.setType("image/png");
		sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
		ctx.startActivity(Intent.createChooser(sharingIntent, null));
	}

	/**
	 * Обновление счетчика на иконке лаунчера
	 * @param context контекст
	 * @param db база данных
	 */
	public static void updateBadgeByUnviewedImages(Context context, AppDatabase db) {
		new Thread(() -> {
			int count = db.getImageDAO().getUnviewedCount();

			Intent i = new Intent(context, UpdateBadgeService.class);
			i.putExtra(UpdateBadgeService.EXTRA_COUNT, count);
			context.startService(i);
		}).start();
	}

}
