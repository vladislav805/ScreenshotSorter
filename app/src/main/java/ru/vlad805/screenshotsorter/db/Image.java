package ru.vlad805.screenshotsorter.db;

import android.annotation.SuppressLint;
import android.arch.persistence.room.*;
import android.os.Parcel;
import android.os.Parcelable;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * vlad805 (c) 2018
 */
@SuppressWarnings("WeakerAccess")
@Entity(
		foreignKeys = @ForeignKey(
				entity = Folder.class,
				parentColumns = "folderId",
				childColumns = "folderId",
				onUpdate = CASCADE,
				onDelete = CASCADE
		)
)
public class Image implements Parcelable {

	@PrimaryKey(autoGenerate = true)
	private int imageId;

	@ColumnInfo(index = true)
	private int folderId;

	private String path;

	private int isViewed = 0;

	private long dateAdded;

	private String description = "";

	public Image(String path) {
		this.path = path;
	}

	protected Image(Parcel in) {
		imageId = in.readInt();
		folderId = in.readInt();
		path = in.readString();
		isViewed = in.readInt();
		description = in.readString();
		dateAdded = in.readLong();
	}

	public int getImageId() {
		return imageId;
	}

	public int getFolderId() {
		return folderId;
	}

	public String getPath() {
		return path;
	}

	public int isViewed() {
		return isViewed;
	}

	public String getDescription() {
		return description;
	}

	public long getDateAdded() {
		return dateAdded;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public void setFolderId(int folderId) {
		this.folderId = folderId;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void isViewed(int viewed) {
		isViewed = viewed;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDateAdded(long dateAdded) {
		this.dateAdded = dateAdded;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public String toString() {
		return String.format("Image { iid=%d, fid=%d, path = %s, isViewed=%d, description=%s, dateAdded=%d}", imageId, folderId, path, isViewed, description, dateAdded);
	}

	public static final Creator<Image> CREATOR = new Creator<Image>() {
		@Override
		public Image createFromParcel(Parcel in) {
			return new Image(in);
		}

		@Override
		public Image[] newArray(int size) {
			return new Image[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(imageId);
		parcel.writeInt(folderId);
		parcel.writeString(path);
		parcel.writeInt(isViewed);
		parcel.writeString(description);
		parcel.writeLong(dateAdded);
	}
}
