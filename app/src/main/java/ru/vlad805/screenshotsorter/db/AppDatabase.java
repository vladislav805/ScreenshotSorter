package ru.vlad805.screenshotsorter.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import ru.vlad805.screenshotsorter.Config;

/**
 * vlad805 (c) 2018
 */
@Database(entities = {Folder.class, Image.class}, version = 5)
public abstract class AppDatabase extends RoomDatabase {

	private static final String TAG = "AppDatabase";
	private static AppDatabase sInstance;

	public abstract FolderDAO getFolderDAO();
	public abstract ImageDAO getImageDAO();

	public static synchronized AppDatabase getInstance(Context context) {

		if (sInstance == null) {
			sInstance = buildDatabase(context.getApplicationContext());
		}

		return sInstance;
	}

	private static AppDatabase buildDatabase(Context context) {
		return Room
				.databaseBuilder(context.getApplicationContext(), AppDatabase.class, Config.DB_NAME)
				.addMigrations(MIGRATION_3_4)
				.addMigrations(MIGRATION_4_5)
				.addCallback(new Callback() {
					@Override
					public void onCreate(@NonNull SupportSQLiteDatabase db) {
						super.onCreate(db);
						Log.i(TAG, "onCreate: start");
						synchronized (this) {
							Log.i(TAG, "onCreate: in sync");
							new Thread(() -> {
								Log.i(TAG, "onCreate: thread start");
								Folder f = new Folder("See later", 0);
								f.setIsSystem(Folder.SYSTEM_SEE_LATER);
								AppDatabase a = getInstance(context);
								a.getFolderDAO().insert(f);
								Log.i(TAG, "onCreate: thread end");
							}).start();
						}
					}

					public void onOpen(@NonNull SupportSQLiteDatabase db) {}
				})
				.build();
	}

	private static final Migration MIGRATION_3_4 = new Migration(3, 4) {
		@Override
		public void migrate(final SupportSQLiteDatabase database) {
			database.execSQL("ALTER TABLE `folder` ADD COLUMN `count` INTEGER DEFAULT 0 NOT NULL");
		}
	};
	private static final Migration MIGRATION_4_5 = new Migration(4, 5) {
		@Override
		public void migrate(final SupportSQLiteDatabase database) {
			database.execSQL("ALTER TABLE `folder` ADD COLUMN `isSystem` INTEGER DEFAULT 0 NOT NULL");
		}
	};
}