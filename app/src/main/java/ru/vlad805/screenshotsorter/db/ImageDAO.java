package ru.vlad805.screenshotsorter.db;

import android.arch.persistence.room.*;

import java.util.List;

@Dao
public interface ImageDAO {

	@Insert
	void insert(Image... images);

	@Update
	int update(Image... images);

	@Delete
	int delete(Image... image);

	@Query("DELETE FROM `image` WHERE `imageId` = :imageId")
	int delete(int imageId);

	@Query("SELECT * FROM `image`")
	List<Image> getAll();

	@Query("SELECT * FROM `image` WHERE `folderId` = :folderId ORDER BY `imageId` DESC")
	List<Image> getByFolderId(int folderId);

	@Query("SELECT COUNT(*) FROM `image` WHERE `isViewed` = 0")
	int getUnviewedCount();

	@Query("SELECT COUNT(*) FROM `image` WHERE `path` = :path")
	int hasImage(String path);
}