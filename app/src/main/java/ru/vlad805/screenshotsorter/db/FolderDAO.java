package ru.vlad805.screenshotsorter.db;

import android.arch.persistence.room.*;

import java.util.List;

@Dao
public interface FolderDAO {

	@Insert
	void insert(Folder... folders);

	@Update
	int update(Folder... folders);

	@Delete
	int delete(Folder folder);

	@Query("SELECT * FROM `folder` WHERE `folderId` <> 0 ORDER BY `order_num` ASC, `isSystem` DESC")
	List<Folder> getAll();

	@Query("SELECT `f`.*, CASE WHEN `i`.`count` IS NOT NULL THEN `i`.`count` ELSE 0 END AS `count` FROM `folder` `f` LEFT JOIN (SELECT `folderId`, COUNT(*) AS `count` FROM `image` WHERE `isViewed` = 0 GROUP BY `folderId`) `i` ON `f`.`folderId` = `i`.`folderId` ORDER BY `order_num` ASC, `isSystem` DESC")
	List<Folder> getAllWithCounter();

	@Query("SELECT * FROM `folder` WHERE `isSystem` > 0 ORDER BY `order_num` ASC")
	List<Folder> getSystemFolders();

	@Query("SELECT * FROM `folder` WHERE `isSystem` = 0 ORDER BY `order_num` ASC")
	List<Folder> getUserFolders();

}