package ru.vlad805.screenshotsorter.db;

import android.annotation.SuppressLint;
import android.arch.persistence.room.*;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * vlad805 (c) 2018
 */
@SuppressWarnings("WeakerAccess")
@Entity
public class Folder implements Parcelable {

	@Ignore
	public static final int SYSTEM_SEE_LATER = 1;

	@PrimaryKey(autoGenerate = true)
	private int folderId;

	private String title;

	private int count = 0;

	@ColumnInfo(name = "isSystem")
	private int mIsSystem = 0;

	@ColumnInfo(name = "order_num")
	private int order = 1;

	public Folder(String title, int order) {
		setTitle(title);
		setOrder(order);
	}

	protected Folder(Parcel in) {
		folderId = in.readInt();
		title = in.readString();
		count = in.readInt();
		mIsSystem = in.readInt();
		order = in.readInt();
	}

	public static final Creator<Folder> CREATOR = new Creator<Folder>() {
		@Override
		public Folder createFromParcel(Parcel in) {
			return new Folder(in);
		}

		@Override
		public Folder[] newArray(int size) {
			return new Folder[size];
		}
	};

	public int getFolderId() {
		return folderId;
	}

	public String getTitle() {
		return title;
	}

	public int getCount() {
		return count;
	}

	public int isSystem() {
		return mIsSystem;
	}

	public int getOrder() {
		return order;
	}

	public void setFolderId(int folderId) {
		this.folderId = folderId;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setIsSystem(int isSystem) {
		mIsSystem = isSystem;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public String toString() {
		return String.format("Folder { fid=%d, title=%s, count=%d, isSystem=%d, order=%d }", folderId, title, count, mIsSystem, order);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(folderId);
		parcel.writeString(title);
		parcel.writeInt(count);
		parcel.writeInt(mIsSystem);
		parcel.writeInt(order);
	}
}
