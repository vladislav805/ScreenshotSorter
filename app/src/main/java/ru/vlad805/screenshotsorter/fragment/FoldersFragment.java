package ru.vlad805.screenshotsorter.fragment;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Pair;
import android.util.SparseArray;
import android.view.*;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;
import ru.vlad805.screenshotsorter.*;
import ru.vlad805.screenshotsorter.activity.GalleryActivity;
import ru.vlad805.screenshotsorter.db.AppDatabase;
import ru.vlad805.screenshotsorter.db.Folder;
import ru.vlad805.screenshotsorter.service.ListenerService;
import ru.vlad805.screenshotsorter.ui.FolderViewAdapter;
import ru.vlad805.screenshotsorter.ui.OnStartDragListener;
import ru.vlad805.screenshotsorter.ui.RecyclerItemClickListener;

import java.util.List;
import java.util.Objects;

public class FoldersFragment extends Fragment implements RecyclerItemClickListener.OnItemClickListener, OnStartDragListener {

	@SuppressWarnings("unused")
	private static final String TAG = FoldersFragment.class.getName();
	private View mRootView;
	private RecyclerView mList;
	private List<Folder> mItems;
	private AppDatabase mDB;
	private FolderViewAdapter mAdapter;
	private ItemTouchHelper mItemTouchHelper;

	private Utils mUtils;
	private Switch mActionBarSwitch;

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent == null || intent.getAction() == null) {
				return;
			}

			switch (intent.getAction()) {
				case ListenerService.ACTION_CHANGE_STATE_SERVICE:
					boolean state = intent.getBooleanExtra(ListenerService.STATE_SERVICE, false);
					mActionBarSwitch.setChecked(state);
					break;
			}

		}
	};

	public FoldersFragment() {

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mUtils = new Utils(getContext());
		mDB = AppDatabase.getInstance(getContext());

		IntentFilter filter = new IntentFilter();
		filter.addAction(ListenerService.ACTION_CHANGE_STATE_SERVICE);
		Objects.requireNonNull(getContext()).registerReceiver(mReceiver, filter);

		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_folders, container, false);

		mList = mRootView.findViewById(R.id.folders_list_items);

		LinearLayoutManager lm = new LinearLayoutManager(getContext());
		DividerItemDecoration divider = new DividerItemDecoration(mRootView.getContext(), lm.getOrientation());

		mList.setLayoutManager(lm);
		mList.addItemDecoration(divider);

		mAdapter = new FolderViewAdapter(this);
		mAdapter.setOnReorderFolders(this::applyMoves);
		mAdapter.setOnLongClickListener(this::onItemLongClick);

		mItemTouchHelper = new ItemTouchHelper(new SimpleItemTouchHelperCallback(mAdapter));
		mItemTouchHelper.attachToRecyclerView(mList);

		mList.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), mList, this));

		return mRootView;
	}

	private void applyMoves(List<Pair<Integer, Integer>> moves) {
		SparseArray<Folder> map = new SparseArray<>();

		for (int i = 0, l = mItems.size(); i < l; ++i) {
			map.append(i, mItems.get(i));
		}

		for (Pair<Integer, Integer> item : moves) {
			int fromPosition = item.first;
			int toPosition = item.second;

			Folder tmp = map.get(toPosition);
			map.setValueAt(toPosition, map.valueAt(fromPosition));
			map.setValueAt(toPosition, tmp);
		}

		mItems.clear();

		for (int i = 0, l = map.size(); i < l; ++i) {
			Folder f = map.valueAt(map.keyAt(i));
			f.setOrder(i);
			mItems.add(f);
		}

		new Thread(() -> mDB.getFolderDAO().update(mItems.toArray(new Folder[0]))).start();
	}

	@Override
	public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
		mItemTouchHelper.startDrag(viewHolder);
	}

	@Override
	public void onResume() {
		super.onResume();

		update(Event.GET, 0);
	}

	private enum Event { GET, INSERT, UPDATE, DELETE }

	private void update(Event e, int pos) {
		new Thread(() -> {
			mItems = mDB.getFolderDAO().getAllWithCounter();
			Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
				if (mAdapter != null) {
					mAdapter.setSource(mItems);
				}

				switch (e) {
					case GET:
						mList.setAdapter(mAdapter);
						mAdapter.notifyDataSetChanged();
						break;

					case INSERT:
						assert mAdapter != null;
						mAdapter.notifyItemInserted(mItems.size() - 1);
						Snackbar.make(mRootView, R.string.folder_add_snackbar_success, Snackbar.LENGTH_SHORT).show();
						break;

					case DELETE:
						assert mAdapter != null;
						mAdapter.notifyItemRemoved(pos);
						break;
				}
			});
		}).start();
	}

	@Override
	public void onItemClick(View view, int position) {
		Intent intent = new Intent(getContext(), GalleryActivity.class);
		intent.putExtra(Config.KEY_FOLDER, mItems.get(position));
		startActivity(intent);
	}

	@Override
	public void onItemLongClick(View view, int position) {

	}

	private void onItemLongClick(int position) {
		Folder folder = mItems.get(position);

		if (folder.isSystem() > 0) {
			return;
		}

		new AlertDialog.Builder(getContext())
				.setTitle(folder.getTitle())
				.setItems(R.array.folder_actions, (dialogInterface, i) -> {
					switch (i) {
						case 0: renameFolder(folder, position); break;
						case 1: removeFolder(folder); break;
					}
				})
				.create()
				.show();
	}

	private void renameFolder(Folder f, int position) {
		View view = getLayoutInflater().inflate(R.layout.dialog_create_folder, null, false);
		EditText etView = view.findViewById(R.id.create_folder_field);
		AlertDialog.Builder ab = new AlertDialog.Builder(getContext())
				.setTitle(R.string.folder_dialog_name_title)
				.setView(view)
				.setPositiveButton(android.R.string.ok, (dialog, which) -> {
					String name = etView.getText().toString().trim();

					if (name.length() == 0) {
						Toast.makeText(view.getContext(), R.string.folder_dialog_name_error_empty, Toast.LENGTH_SHORT).show();
						return;
					}

					f.setTitle(name);
					try {
						Thread d = new Thread(() -> mDB.getFolderDAO().update(f));
						d.start();
						d.join();
						update(Event.UPDATE, position);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				})
				.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());
		etView.setText(f.getTitle());
		ab.create().show();
	}

	private void removeFolder(Folder f) {
		Utils.confirm(getContext(), R.string.folder_remove_title, R.string.folder_remove_text, android.R.string.yes, android.R.string.cancel, 0, () -> new Thread(() -> {
			mDB.getFolderDAO().delete(f);

			update(Event.DELETE, mItems.indexOf(f));
		}).start());
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.folders, menu);

		mActionBarSwitch = (Switch) menu.findItem(R.id.navigation_switch_state).getActionView();

		mActionBarSwitch.setChecked(mUtils.isServiceRunning(ListenerService.class));
		mActionBarSwitch.setOnCheckedChangeListener(this::toggleService);

		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.navigation_folders_add:
				showCreateFolderDialog();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void toggleService(CompoundButton view, boolean b) {
		Context c = view.getContext();
		Intent i = new Intent(c, ListenerService.class);
		if (b) {
			c.startService(i);
			i = new Intent(ListenerService.ACTION_CHANGE_STATE_SERVICE);
			i.putExtra(ListenerService.STATE_SERVICE, true);
			c.sendBroadcast(i);
		} else{
			c.stopService(i);
		}
	}

	@SuppressLint("InflateParams")
	private void showCreateFolderDialog() {
		View view = getLayoutInflater().inflate(R.layout.dialog_create_folder, null, false);
		AlertDialog.Builder ab = new AlertDialog.Builder(getContext())
				.setTitle(R.string.folder_dialog_name_title)
				.setView(view)
				.setPositiveButton(android.R.string.ok, (dialog, which) -> {
					String name = ((EditText) view.findViewById(R.id.create_folder_field)).getText().toString().trim();

					if (name.length() == 0) {
						Toast.makeText(view.getContext(), R.string.folder_dialog_name_error_empty, Toast.LENGTH_SHORT).show();
						return;
					}

					new Thread(() -> {
						int order = mItems.size();
						mDB.getFolderDAO().insert(new Folder(name, order));
						update(Event.INSERT, -1);
					}).start();
				})
				.setNegativeButton(android.R.string.cancel, (dialog, which) -> dialog.cancel());
		ab.create().show();
	}

	@Override
	public void onDestroy() {
		if (mReceiver != null && getContext() != null) {
			getContext().unregisterReceiver(mReceiver);
			mReceiver = null;
		}
		super.onDestroy();
	}
}
