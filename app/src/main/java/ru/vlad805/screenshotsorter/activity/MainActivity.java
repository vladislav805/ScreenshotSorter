package ru.vlad805.screenshotsorter.activity;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.RuntimePermissions;
import ru.vlad805.screenshotsorter.R;
import ru.vlad805.screenshotsorter.db.AppDatabase;
import ru.vlad805.screenshotsorter.fragment.FoldersFragment;

@RuntimePermissions
public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		new Thread(() -> AppDatabase.getInstance(this).getFolderDAO().getAll()).start();
		MainActivityPermissionsDispatcher.startWithPermissionCheck(this);
	}

	@NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
	void start() {
		getSupportFragmentManager().beginTransaction().replace(R.id.main_fragment, new FoldersFragment()).commit();
	}

	@OnPermissionDenied({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
	void permissionAccessDenied() {
		Toast.makeText(this, R.string.access_rights_android, Toast.LENGTH_LONG).show();
		finish();
	}

	@OnNeverAskAgain({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
	void permissionNeverAsk() {
		Toast.makeText(this, "never ask", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
	}

}
