package ru.vlad805.screenshotsorter.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.stfalcon.frescoimageviewer.ImageViewer;
import ru.vlad805.screenshotsorter.BuildConfig;
import ru.vlad805.screenshotsorter.Config;
import ru.vlad805.screenshotsorter.R;
import ru.vlad805.screenshotsorter.Utils;
import ru.vlad805.screenshotsorter.db.AppDatabase;
import ru.vlad805.screenshotsorter.db.Folder;
import ru.vlad805.screenshotsorter.db.Image;
import ru.vlad805.screenshotsorter.ui.GalleryViewAdapter;
import ru.vlad805.screenshotsorter.ui.ImageOverlayView;
import ru.vlad805.screenshotsorter.ui.RecyclerItemClickListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GalleryActivity extends AppCompatActivity implements RecyclerItemClickListener.OnItemClickListener, ImageOverlayView.OnDescriptionEditedListener {

	private static final String TAG = "GalleryActivity";

	private static final int EDIT_IMAGE_RESULT_CODE = 14952;
	private static final int EDIT_DESCRIPTION_RESULT_CODE = 0xed11;

	/**
	 * Список фотографий в папке
	 */
	private List<Image> mItems;

	/**
	 * Ссылка на базу данных
	 */
	private AppDatabase mDB;

	/**
	 * Данные об открытой папке
	 */
	private Folder mFolder;

	/**
	 * Текущая просматриваемая фотография
	 */
	private int mCurrentIndex = -1;

	/**
	 * Тулбар галереи
	 */
	private ActionBar mActionBar;

	/**
	 * Лейбл с текстом о том, что папка пуста
	 */
	private TextView mLabelEmpty;

	/**
	 * RecyclerView для галереи
	 */
	private RecyclerView mRV;
	private GalleryViewAdapter mAdapter;

	/**
	 * Callback, вызываемый при закрытии просмоторщика
	 */
	private ImageViewer.OnDismissListener mOnDismiss = () -> mCurrentIndex = -1;

	private ActionMode currentActionMode;

	private ActionMode.Callback modeCallBack = new ActionMode.Callback() {

		// Called when the action mode is created; startActionMode() was called
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			mode.setTitle(R.string.gallery_action_mode_title);
			mode.getMenuInflater().inflate(R.menu.gallery_selection, menu);
			mAdapter.startActionMode();
			return true;
		}

		// Called each time the action mode is shown.
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		// Called when the user selects a contextual menu item
		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			Image[] selected = mAdapter.getSelectedItems();

			if (selected.length == 0) {
				mode.finish();
				Toast.makeText(GalleryActivity.this, R.string.gallery_action_mode_nothing_selected, Toast.LENGTH_SHORT).show();
				return true;
			}

			switch (item.getItemId()) {
				case R.id.gallery_action_share:
					shareImages(selected);
					mode.finish();
					return true;

				case R.id.gallery_action_delete:
					Utils.showRemoveFilesDialog(GalleryActivity.this, mDB, selected, alsoFile -> fetchThread());
					mode.finish();
					return true;

				case R.id.gallery_action_mark_unviewed:
					mode.finish();
					for (Image i : selected) {
						i.isViewed(0);
					}

					try {
						Thread t = new Thread(() -> mDB.getImageDAO().update(selected));
						t.start();
						t.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					fetchThread();
					return true;

				default:
					return false;
			}
		}

		// Called when the user exits the action mode
		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mAdapter.destroyActionMode();
			currentActionMode = null; // Clear current action mode
		}
	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery);

		Intent i = getIntent();

		if (i == null) {
			return;
		}

		mFolder = i.getParcelableExtra(Config.KEY_FOLDER);

		if (mFolder == null) {
			Toast.makeText(this, R.string.gallery_error_unknown_folder, Toast.LENGTH_LONG).show();
			finish();
			return;
		}

		mActionBar = getSupportActionBar();

		if (mActionBar != null) {
			mActionBar.setDisplayHomeAsUpEnabled(true);
			mActionBar.setTitle(mFolder.getTitle());
			mActionBar.setSubtitle(R.string.gallery_subtitle_fetching);
			mActionBar.setDisplayShowHomeEnabled(true);
		}

		mRV = findViewById(R.id.gallery_view);
		mLabelEmpty = findViewById(R.id.gallery_empty_label);

		mRV.setLayoutManager(new GridLayoutManager(GalleryActivity.this, GalleryViewAdapter.COLUMN_COUNT));
		mRV.addOnItemTouchListener(new RecyclerItemClickListener(GalleryActivity.this, mRV, GalleryActivity.this));

		mDB = AppDatabase.getInstance(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		fetchThread();
	}

	private void fetchThread() {
		new Thread(this::fetchData).start();
	}

	/**
	 * Запрос данных из БД
	 * Вызывается в отдельном потоке
	 */
	private void fetchData() {
		mDB.beginTransaction();
		mItems = mDB.getImageDAO().getByFolderId(mFolder.getFolderId());
		mDB.endTransaction();

		Iterator i = mItems.iterator();

		while (i.hasNext()) {
			Image img = ((Image) i.next());
			if (!new File(img.getPath()).exists()) {
				mDB.getImageDAO().delete(img);
				i.remove();
			}
		}

		runOnUiThread(this::showItems);
	}

	/**
	 * Вывод в UI-потоке фотографий в RecyclerView
	 */
	private void showItems() {
		int s = mItems.size();
		if (s > 0) {
			DisplayMetrics metrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(metrics);
			mAdapter = new GalleryViewAdapter(mItems, metrics);
			mRV.setAdapter(mAdapter);
		} else {
			mLabelEmpty.setVisibility(View.VISIBLE);
			mRV.setVisibility(View.GONE);
		}
		if (mActionBar != null) {
			setImagesCountInActionBar(s);
		}
	}

	/**
	 * Изменение строки с количеством изображений в тулбаре после загрузки данных и БД
	 * @param s количество изображений
	 */
	private void setImagesCountInActionBar(int s) {
		if (mActionBar != null) {
			if (s > 0) {
				mActionBar.setSubtitle(getResources().getQuantityString(R.plurals.gallery_subtitle_images, s, s));
			} else {
				mActionBar.setSubtitle(R.string.gallery_subtitle_nothing);
			}
		}
	}

	@Override
	public void onItemClick(View v, int index) {
		if (currentActionMode == null) {
			showImage(index);
		} else {
			mAdapter.setSelectionItem(index);
		}
	}

	@Override
	public void onItemLongClick(View view, final int position) {
		if (currentActionMode != null) {
			return;
		}

		currentActionMode = startSupportActionMode(modeCallBack);
		mAdapter.setSelectionItem(position, true);
	}

	private void showImage(int index) {
		ImageOverlayView overlay = new ImageOverlayView(this);
		ImageViewer iv = new ImageViewer.Builder<>(this, mItems)
				.setFormatter(this::getPathUrl)
				.setStartPosition(index)
				.hideStatusBar(false)
				.setImageChangeListener(position -> setInfoInViewer(overlay, mCurrentIndex = position))
				.setOnDismissListener(mOnDismiss)
				.setOverlayView(overlay)
				.show();
		overlay.setAllClicksListener(id -> {
			switch (id) {
				case android.R.id.home:
					iv.onDismiss();
					break;

				case ImageOverlayView.BUTTON_DELETE:
					deleteImage();
					iv.onDismiss();
					break;

				case ImageOverlayView.BUTTON_SHARE:
					shareImage();
					break;

				case ImageOverlayView.BUTTON_EDIT:
					iv.onDismiss();
					editImage();
					break;

				case ImageOverlayView.BUTTON_DESCRIPTION:
					editDescription();
					break;

				case ImageOverlayView.BUTTON_TOGGLE_UNVIEWED:
					setAsViewed(mCurrentIndex, State.UNVIEWED);
					break;

			}

		}, this);
	}

	/**
	 * Изменение информации об изображении во время пролистывания их в просмоторщике
	 * @param overlay оверлей
	 * @param position индекс изображения в массиве с фотками
	 */
	@SuppressLint("DefaultLocale")
	private void setInfoInViewer(ImageOverlayView overlay, int position) {
		Image i = mItems.get(position);
		overlay.setTitle(String.format("%d/%d", position + 1, mItems.size()))
				.setSubtitle(Utils.getDateString(i.getDateAdded()))
				.setDescription(i.getDescription());
		setAsViewed(position, State.VIEWED);
	}

	public enum State {
		UNVIEWED(0),
		VIEWED(1),
		TOGGLE(-1);

		private int val;

		State(int s) {
			val = s;
		}

		public int get() {
			return val;
		}

	}

	/**
	 * Попытка изменить состояние изображения
	 * Если оно уже просмотренное, запрос не производится (логично, правда?)
	 * @param position индекс изображения в массиве
	 */
	private void setAsViewed(int position, State state) {
		Image i = mItems.get(position);

		if (state != State.TOGGLE && i.isViewed() == state.get()) {
			return;
		}

		State newValue = state != State.TOGGLE
				? state
				: (i.isViewed() == State.UNVIEWED.get() ? State.VIEWED : State.UNVIEWED);

		i.isViewed(newValue.get());

		mAdapter.notifyItemChanged(position);

		try {
			Thread t = new Thread(() -> mDB.getImageDAO().update(i));
			t.start();
			t.join();
		} catch (InterruptedException ignored) {}

		Utils.updateBadgeByUnviewedImages(this, mDB);
	}

	/**
	 * Открытие выбора шаринга
	 */
	private void shareImage() {
		Utils.openShareIntent(this, mItems.get(mCurrentIndex).getPath());
	}

	/**
	 * Открытие выбора шаринга
	 */
	private void shareImages(Image[] images) {
		ArrayList<Uri> uris = new ArrayList<>();

		for (Image image : images) {
			uris.add(FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID, new File(image.getPath())));
		}

		Intent sharingIntent = new Intent();
		sharingIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
		sharingIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
		sharingIntent.setType("image/*");
		sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
		startActivity(Intent.createChooser(sharingIntent, null));
	}

	private void deleteImage() {
		deleteImage(mCurrentIndex);
	}

	private void deleteImage(int position) {
		Utils.showRemoveFileDialog(GalleryActivity.this, mDB, mItems.get(position), alsoFile -> onRemovedImage(alsoFile, position));
	}

	private void onRemovedImage(boolean alsoFile, int position) {
		runOnUiThread(() -> {
			int str = !alsoFile ? R.string.gallery_remove_toast_success : R.string.gallery_remove_toast_success_with_file;

			Toast.makeText(GalleryActivity.this, str, Toast.LENGTH_SHORT).show();
			mItems.remove(position);
			mAdapter.notifyItemRemoved(position);
			GalleryActivity.this.setImagesCountInActionBar(mItems.size());
		});
	}

	@Override
	public void onDescriptionEdited(String description) {
		if (mCurrentIndex < 0) {
			return;
		}

		Image image = mItems.get(mCurrentIndex);
		image.setDescription(description);
		new Thread(() -> mDB.getImageDAO().update(image)).start();
	}

	private void editImage() {
		Utils.openImageEditor(this, mItems.get(mCurrentIndex).getPath(), EDIT_IMAGE_RESULT_CODE);
	}

	private void editDescription() {
		if (mCurrentIndex < 0) {
			return;
		}

		Image img = mItems.get(mCurrentIndex);

		Intent i = new Intent(this, EditDescriptionActivity.class);
		i.putExtra(Intent.EXTRA_STREAM, img);
		i.putExtra(Intent.EXTRA_TITLE, getString(R.string.folder_dialog_name_title));
		i.putExtra(Intent.EXTRA_TEXT, img.getDescription());
		startActivityForResult(i, EDIT_DESCRIPTION_RESULT_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case EDIT_IMAGE_RESULT_CODE:
				if (resultCode == RESULT_OK) {
					if (data.getData() == null) {
						Toast.makeText(this, "getData() == null", Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(this, "getData() != null; " + data.getData().getPath(), Toast.LENGTH_SHORT).show();
					}
					Toast.makeText(this, R.string.screen_action_edit_successfully, Toast.LENGTH_SHORT).show();
					if (mCurrentIndex < 0) {
						return;
					}
					new Thread(() -> {
						runOnUiThread(() -> mAdapter.notifyItemChanged(mCurrentIndex));
					}).start();

				}
				break;

			case EDIT_DESCRIPTION_RESULT_CODE:
				if (resultCode != RESULT_OK) {
					return;
				}
				Image img = data.getParcelableExtra(Intent.EXTRA_STREAM);
				img.setDescription(data.getStringExtra(Intent.EXTRA_TEXT));
				new Thread(() -> mDB.getImageDAO().update(img)).start();
				break;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Получение реального пути по Uri
	 * @param image объект изображения
	 * @return реальный путь
	 */
	private String getPathUrl(Image image) {
		return Uri.fromFile(new File(image.getPath())).toString();
	}
}
