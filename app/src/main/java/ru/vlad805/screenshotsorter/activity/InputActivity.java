package ru.vlad805.screenshotsorter.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import ru.vlad805.screenshotsorter.Config;
import ru.vlad805.screenshotsorter.R;

public class InputActivity extends AppCompatActivity implements View.OnClickListener {

	protected EditText mText;
	protected Button mButtonOk;
	protected Button mButtonCancel;
	protected Button mButtonExtra;

	protected View.OnClickListener mExtraListener;

	protected Intent mIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_input);

		mText = findViewById(R.id.dialog_input);
		mButtonOk = findViewById(R.id.dialog_button_ok);
		mButtonCancel = findViewById(R.id.dialog_button_cancel);
		mButtonExtra = findViewById(R.id.dialog_button_extra);

		mIntent = getIntent();

		init();
		getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(this::adjustDialog);
	}

	@Override
	protected void onResume() {
		super.onResume();

		mText.requestFocus();
	}

	private void init() {
		if (mIntent.getBooleanExtra(Config.DIALOG_EXTRA_BUTTON, false) && mIntent.getStringExtra(Config.DIALOG_EXTRA_BUTTON_LABEL) != null) {
			setVisibleExtraButton(true);
		}

		String str = mIntent.getStringExtra(Intent.EXTRA_TEXT);
		if (str != null) {
			mText.setText(str);
		}

		str = mIntent.getStringExtra(Intent.EXTRA_TITLE);
		if (str != null) {
			setTitle(str);
		}

		mButtonOk.setOnClickListener(this);
		mButtonCancel.setOnClickListener(this);
		mButtonExtra.setOnClickListener(this);
	}

	@SuppressWarnings({"WeakerAccess", "SameParameterValue"})
	protected void setVisibleExtraButton(boolean state) {
		mButtonExtra.setVisibility(state ? View.VISIBLE : View.GONE);
		mButtonExtra.setText(mIntent.getStringExtra(Config.DIALOG_EXTRA_BUTTON_LABEL));
	}

	private void adjustDialog() {
		Window w = getWindow();
		w.setGravity(Gravity.CENTER);

		int current_width = w.getDecorView().getWidth();
		int current_height = w.getDecorView().getHeight();

		WindowManager.LayoutParams lp = w.getAttributes();

		DisplayMetrics dm = getResources().getDisplayMetrics();
		int max_width = (int) (dm.widthPixels * 0.90);
		int max_height = (int) (dm.heightPixels * 0.90);

		if (current_width > max_width) {
			lp.width = max_width;
		}
		if (current_height > max_height) {
			lp.height = max_height;
		}

		w.setAttributes(lp);
	}

	@SuppressWarnings("WeakerAccess")
	public void setExtraButtonClickListener(View.OnClickListener listener) {
		mExtraListener = listener;
	}

	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.dialog_button_ok:
				mIntent.putExtra(Intent.EXTRA_TEXT, mText.getText().toString());

				setResult(RESULT_OK, mIntent);
				finish();
				break;

			case R.id.dialog_button_cancel:
				setResult(RESULT_CANCELED, mIntent);
				finish();
				break;

			case R.id.dialog_button_extra:
				if (mExtraListener != null) {
					mExtraListener.onClick(mText);
				}
				break;
		}
	}
}
