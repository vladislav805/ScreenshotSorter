package ru.vlad805.screenshotsorter.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import org.jetbrains.annotations.NotNull;
import ru.vlad805.screenshotsorter.Config;
import ru.vlad805.screenshotsorter.R;

public class EditDescriptionActivity extends InputActivity implements View.OnClickListener {

	private View.OnClickListener mExtraClick = v -> {
		CharSequence text = getClipboardData().getItemAt(0).getText();
		mText.setText(text);
		mText.setSelection(text.length());
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getClipboardData().getItemCount() > 0) {
			setVisibleExtraButton(true);
			mButtonExtra.setText(R.string.screen_description_paste);
			setExtraButtonClickListener(mExtraClick);
		}
	}

	@NotNull
	private ClipData getClipboardData() {
		ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
		assert clipboardManager != null;
		return clipboardManager.getPrimaryClip();
	}

	@Override
	protected void onResume() {
		super.onResume();

		mText.requestFocus();
	}

}
