package ru.vlad805.screenshotsorter.activity;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import ru.vlad805.screenshotsorter.*;
import ru.vlad805.screenshotsorter.db.AppDatabase;
import ru.vlad805.screenshotsorter.db.Folder;
import ru.vlad805.screenshotsorter.db.Image;
import ru.vlad805.screenshotsorter.ui.FolderViewAdapter;
import ru.vlad805.screenshotsorter.ui.RecyclerItemClickListener;

import java.io.*;
import java.util.List;

public class ScreenActivity extends AppCompatActivity implements View.OnClickListener, RecyclerItemClickListener.OnItemClickListener {

	private static final String TAG = ScreenActivity.class.getName();
	private static final int EDIT_IMAGE_RESULT_CODE = 0xED11;
	private static final int FOLDER_NOT_SELECTED = -1;
	private static final int FOLDER_CANCELLED = -2;

	private static final int ERROR_EMPTY_EXTRAS = 1;
	private static final int ERROR_PATH_EMPTY = 2;
	private static final int ERROR_FILE_NOT_FOUND = 3;
	private static final int ERROR_INPUT_STREAM_NULL = 4;
	private static final int ERROR_IO_EXCEPTION = 5;
	private static final int REQUEST_CODE_DESCRIPTION = 0xed1d;

	private int mError = 0;

	private String mPath;
	private String mDescription;

	private RecyclerView mList;
	private List<Folder> mItems;
	private AppDatabase mDB;

	private int mTargetFolder = FOLDER_NOT_SELECTED;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_screen);

		Intent i = getIntent();

		if (i == null) {
			showError();
			return;
		}

		Bundle extras = i.getExtras();

		if (extras == null) {
			mError = ERROR_EMPTY_EXTRAS;
			showError();
			return;
		}

		if (extras.containsKey(Config.KEY_PATH)) {
			mPath = extras.getString(Config.KEY_PATH);
		} else if (extras.containsKey(Intent.EXTRA_STREAM)) {
			mPath = PathUtil.getPath(this, (Uri) extras.get(Intent.EXTRA_STREAM));
		} else {
			return;
		}

		if (mPath == null) {
			try {
				Log.i(TAG, "onCreate: path is null");
				Uri uri = (Uri) extras.get(Intent.EXTRA_STREAM);
				assert uri != null;

				String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + BuildConfig.APPLICATION_ID;

				File dir = new File(path);
				if (!dir.exists()) {
					//noinspection ResultOfMethodCallIgnored
					dir.mkdirs();
				}

				path += File.separator + System.currentTimeMillis() + ".png";

				InputStream is = getContentResolver().openInputStream(uri);
				Log.i(TAG, "onCreate: attempt for copy file");
				if (is == null) {
					mError = ERROR_INPUT_STREAM_NULL;
					showError();
					return;
				}

				OutputStream os = new FileOutputStream(new File(path));
				Log.i(TAG, "onCreate: copying file...");

				Utils.copy(is, os);

				mPath = path;
				Log.i(TAG, "onCreate: new path: " + path);
			} catch (FileNotFoundException e) {
				mError = ERROR_FILE_NOT_FOUND;
				showError();
				return;
			} catch (IOException e) {
				mError = ERROR_IO_EXCEPTION;
				showError();
				return;
			}

			if (mPath == null) {
				mError = ERROR_PATH_EMPTY;

				showError();
				return;
			}
		}

		loadAndShowImage();

		mList = findViewById(R.id.screen_folder_list);

		LinearLayoutManager lm = new LinearLayoutManager(this);
		DividerItemDecoration divider = new DividerItemDecoration(this, lm.getOrientation());

		mList.setLayoutManager(lm);
		mList.addItemDecoration(divider);

		mList.addOnItemTouchListener(new RecyclerItemClickListener(this, mList, this));

		mDB = AppDatabase.getInstance(this);

		new Thread(() -> {
			mDB.beginTransaction();
			mItems = mDB.getFolderDAO().getAll();
			mDB.endTransaction();
			FolderViewAdapter ad = new FolderViewAdapter(null);
			ad.setSource(mItems);
			ad.setLiteMode(true);
			runOnUiThread(() -> mList.setAdapter(ad));
		}).start();
	}

	private void loadAndShowImage() {
		((ImageView) findViewById(R.id.screen_preview)).setImageDrawable(Drawable.createFromPath(mPath));
	}

	private void showError() {
		Toast.makeText(this, getString(R.string.screen_error, mError), Toast.LENGTH_SHORT).show();
		finish();
	}

	@Override
	public void onClick(View item) {
		switch (item.getId()) {
			case R.id.navigation_screen_menu_share:
				shareImage();
				break;

			case R.id.navigation_screen_menu_text:
				editDescription();
				break;

			case R.id.navigation_screen_menu_edit:
				editImage();
				break;

			case R.id.navigation_screen_menu_delete:
				Utils.removeFile(this, mPath);
				Toast.makeText(this, R.string.screen_action_remove_successfully, Toast.LENGTH_LONG).show();
				mTargetFolder = FOLDER_CANCELLED;
				finish();
				break;
		}
	}

	@Override
	public void onItemClick(View view, int position) {
		Folder folder = mItems.get(position);
		Image image = new Image(mPath);

		mTargetFolder = folder.getFolderId();

		new Thread(new AddImage(folder, image)).start();
		finish();
	}

	@Override
	public void onItemLongClick(View view, int position) {

	}

	private void shareImage() {
		Utils.openShareIntent(this, mPath);
	}


	private void editImage() {
		Utils.openImageEditor(this, mPath, EDIT_IMAGE_RESULT_CODE);
	}

	private void editDescription() {
		Intent s = new Intent(this, EditDescriptionActivity.class);
		s.putExtra(Intent.EXTRA_TITLE, getString(R.string.screen_description_title));
		s.putExtra(Intent.EXTRA_TEXT, mDescription);
		startActivityForResult(s, REQUEST_CODE_DESCRIPTION);
	}

	private class AddImage implements Runnable {

		private Folder mFolder;
		private Image mImage;

		AddImage(Folder f, Image i) {
			mFolder = f;
			mImage = i;
		}

		@Override
		public void run() {
			try {
				mImage.setDescription(mDescription);
				mImage.setDateAdded(System.currentTimeMillis());
				mImage.setFolderId(mFolder.getFolderId());
				mDB.getImageDAO().insert(mImage);
				runOnUiThread(() -> Toast.makeText(ScreenActivity.this, String.format(getString(R.string.screen_toast_moved_to), mFolder.getTitle()), Toast.LENGTH_LONG).show());
			} catch (Exception e) {
				Log.e(TAG, "run: " + e);
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case EDIT_IMAGE_RESULT_CODE:
				Log.i(TAG, "onActivityResult: result = " + resultCode);

				if (resultCode == RESULT_OK) {
					Toast.makeText(this, R.string.screen_action_edit_successfully, Toast.LENGTH_SHORT).show();
					loadAndShowImage();
				}
				break;

			case REQUEST_CODE_DESCRIPTION:
				mDescription = data.getStringExtra(Intent.EXTRA_TEXT);
				break;
		}
	}

	private Runnable mOnActivityDestroyedAndFolderNotSelected = () -> {
		List<Folder> list = mDB.getFolderDAO().getSystemFolders();

		Folder fid = null;

		for (Folder f : list) {
			if (f.isSystem() == Folder.SYSTEM_SEE_LATER) {
				fid = f;
				break;
			}
		}

		if (fid != null) {
			new AddImage(fid, new Image(mPath)).run();
			log("screen added to see later");
		}
	};

	@Override
	protected void onDestroy() {
		log("onDestroy()");
		if (mDB != null && mError == 0 && mTargetFolder == FOLDER_NOT_SELECTED) {
			try {
				Thread t = new Thread(mOnActivityDestroyedAndFolderNotSelected);
				t.start();
				t.join();
			} catch (InterruptedException ignored) {}
		}

		Utils.updateBadgeByUnviewedImages(this, mDB);

		super.onDestroy();
	}

	private void log(String s) {
		if (BuildConfig.DEBUG) {
			runOnUiThread(() -> Toast.makeText(this, s, Toast.LENGTH_SHORT).show());
		}
	}
}
