package ru.vlad805.screenshotsorter.service;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Point;
import android.net.Uri;
import android.os.*;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.ImageColumns;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;
import org.jetbrains.annotations.NotNull;
import ru.vlad805.screenshotsorter.*;
import ru.vlad805.screenshotsorter.activity.ScreenActivity;
import ru.vlad805.screenshotsorter.db.AppDatabase;
import ru.vlad805.screenshotsorter.db.ImageDAO;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ListenerService extends Service {

	private static final boolean DEBUG = false;

	private static final int NOTIFICATION_ID = 0x4f7d1;
	private static final String SCREEN = "screenshot";

	@SuppressWarnings("unused")
	private static final String TAG = "Service";

	public static final String ACTION_CHANGE_STATE_SERVICE = "ru.vlad805.screenshotcapture.action.CHANGE_STATE_SERVICE";

	public static final String STATE_SERVICE = "STATE_SERVICE";
	private static final String CHANNEL_ID = "notification0";

	private String[] mMediaProjections = new String[] {
			ImageColumns.DATA,
			ImageColumns.DISPLAY_NAME,
			ImageColumns.BUCKET_DISPLAY_NAME,
			ImageColumns.DATE_TAKEN,
			ImageColumns.TITLE,
			ImageColumns.WIDTH,
			ImageColumns.HEIGHT
	};

	private long mLastMediaCheckTime;

	private InternalObserver mInternalObserver;
	private ExternalObserver mExternalObserver;
	private StopMediaObserverRunnable mStopMediaObserverRunnable;

	private int mStartObserverToken;

	private NotificationManager mNotificationManager;
	private NotificationCompat.Builder mBuilder;


	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {
		super.onCreate();
		Context context = getApplicationContext();

		mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		assert mNotificationManager != null;

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = mNotificationManager.getNotificationChannel(CHANNEL_ID);

			if (channel == null) {
				channel = new NotificationChannel(CHANNEL_ID, getString(R.string.notification_channel_name), NotificationManager.IMPORTANCE_LOW);
				mNotificationManager.createNotificationChannel(channel);
			}

			mBuilder = populateNotification(new NotificationCompat.Builder(this, CHANNEL_ID));
		} else {
			mBuilder = populateNotification(new NotificationCompat.Builder(this));
		}

		mLastMediaCheckTime = System.currentTimeMillis();

		startMediaObserver();
		updateNotification();
	}

	private NotificationCompat.Builder populateNotification(NotificationCompat.Builder notify) {
		return notify
				.setContentTitle(getString(R.string.app_name))
				.setContentText(getString(R.string.notification_hint))
				.setOngoing(true)
				.setSmallIcon(R.drawable.ic_logo_small);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);

		if (intent == null) {
			return START_STICKY;
		}

		String action = intent.getAction();

		// Startup
		if (action == null) {
			updateNotification();
			broadcastNewState(true);
			return START_STICKY;
		}

		return START_STICKY;
	}

	private void broadcastNewState(boolean state) {
		Intent i = new Intent(ACTION_CHANGE_STATE_SERVICE);
		i.putExtra(STATE_SERVICE, state);
		sendBroadcast(i);
	}

	private void updateNotification() {
		Notification n = mBuilder.build();
		startForeground(NOTIFICATION_ID, n);
		mNotificationManager.notify(NOTIFICATION_ID, n);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@SuppressWarnings("ConstantConditions")
	@Override
	public void onTaskRemoved(Intent rootIntent) {
		Intent restartServiceIntent = new Intent(getApplicationContext(), getClass());
		restartServiceIntent.setPackage(getPackageName());

		PendingIntent restartServicePendingIntent = PendingIntent.getService(
				getApplicationContext(), 1, restartServiceIntent,
				PendingIntent.FLAG_ONE_SHOT);
		((AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE))
				.set(
						AlarmManager.ELAPSED_REALTIME,
						SystemClock.elapsedRealtime() + 1000,
						restartServicePendingIntent
				);

		super.onTaskRemoved(rootIntent);
	}

	@Override
	public void onDestroy() {
		broadcastNewState(false);
		stopForeground(true);
		stopMediaObserver();

		stopSelf();
		super.onDestroy();
	}

	private class InternalObserver extends ContentObserver {
		InternalObserver() {
			super(null);
		}

		@Override
		public void onChange(boolean selfChange) {
			super.onChange(selfChange);
			processMediaObserver(Media.INTERNAL_CONTENT_URI);
		}
	}

	private class ExternalObserver extends ContentObserver {
		ExternalObserver() {
			super(null);
		}

		@Override
		public void onChange(boolean selfChange) {
			super.onChange(selfChange);
			processMediaObserver(Media.EXTERNAL_CONTENT_URI);
		}
	}

	private final class StopMediaObserverRunnable implements Runnable {
		private int currentObserverToken = 0;

		@Override
		public void run() {
			if (currentObserverToken == mStartObserverToken) {
				try {
					if (mInternalObserver != null) {
						ApplicationLoader.applicationContext.getContentResolver().unregisterContentObserver(mInternalObserver);
						mInternalObserver = null;
					}
					if (mExternalObserver != null) {
						ApplicationLoader.applicationContext.getContentResolver().unregisterContentObserver(mExternalObserver);
						mExternalObserver = null;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void startMediaObserver() {
		if (mStopMediaObserverRunnable != null) {
			ApplicationLoader.applicationHandler.removeCallbacks(mStopMediaObserverRunnable);
		}
		mStartObserverToken++;
		try {
			if (mInternalObserver == null) {
				ApplicationLoader.applicationContext.getContentResolver().registerContentObserver(Media.INTERNAL_CONTENT_URI, false, mInternalObserver = new InternalObserver());
			}
			if (mExternalObserver == null) {
				ApplicationLoader.applicationContext.getContentResolver().registerContentObserver(Media.EXTERNAL_CONTENT_URI, false, mExternalObserver = new ExternalObserver());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void stopMediaObserver() {
		if (mStopMediaObserverRunnable == null) {
			mStopMediaObserverRunnable = new StopMediaObserverRunnable();
		}
		mStopMediaObserverRunnable.currentObserverToken = mStartObserverToken;
		ApplicationLoader.applicationHandler.postDelayed(mStopMediaObserverRunnable, 5000);
	}

	private void processMediaObserver(Uri uri) {
		long curr = System.currentTimeMillis() / 1000 - 1;
		new Handler(Looper.getMainLooper()).postDelayed(() -> processMediaObserverPost(uri, curr), 1000);

	}

	private void processMediaObserverPost(Uri uri, long time) {
		try {
			Point size = Utils.getRealScreenSize(this);

			Cursor cursor = ApplicationLoader.applicationContext.getContentResolver().query(
					uri,
					mMediaProjections,
					MediaStore.MediaColumns.DATE_ADDED + " > ?",
					new String[]{ String.valueOf(time)},
					MediaStore.MediaColumns.DATE_ADDED + " DESC LIMIT 1"
			);

			assert cursor != null;
			ScreenshotItem result = null;

			while (cursor.moveToNext()) {
				String path = cursor.getString(0);
				String filename = cursor.getString(1);
				String directory = cursor.getString(2);
				long date = cursor.getLong(3);
				String title = cursor.getString(4);

				if (path == null || !new File(path).exists()) {
					continue;
				}

				if (
						Utils.checkIfSizesEquals(size, new Point(cursor.getInt(5), cursor.getInt(6))) &&
						(
							path.toLowerCase().contains(SCREEN) ||
							filename != null && filename.toLowerCase().contains(SCREEN) ||
							directory != null && directory.toLowerCase().contains(SCREEN) ||
							title != null && title.toLowerCase().contains(SCREEN)
						) &&
						date > mLastMediaCheckTime
				) {
					result = new ScreenshotItem(path, date);
				}
				mLastMediaCheckTime = Math.max(mLastMediaCheckTime, date);
			}
			cursor.close();

			ScreenshotItem finalResult = result;
			new Thread(() -> {
				if (finalResult != null && AppDatabase.getInstance(this).getImageDAO().hasImage(finalResult.getPath()) == 0) {
					processImage(finalResult);
				}
			}).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void processImage(@NotNull ScreenshotItem item) {

		Log.i(TAG, "processImage: " + item);

		Intent i = new Intent(this, ScreenActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
		i.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		i.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		i.putExtra(Config.KEY_PATH, item.getPath());
		startActivity(i);
	}

	@SuppressWarnings("unused")
	private void dbg(Object s) {
		if (BuildConfig.DEBUG && DEBUG) {
			new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(ListenerService.this, s.toString(), Toast.LENGTH_SHORT).show());

		}
	}

	private class ScreenshotItem {

		private String mPath;

		private long mDate;

		ScreenshotItem(String path, long date) {
			mPath = path;
			mDate = date;
		}

		String getPath() {
			return mPath;
		}

		void setPath(String path) {
			mPath = path;
		}

		long getDate() {
			return mDate;
		}

		@Override
		public String toString() {
			return "ScreenshotItem { path=" + getPath() + ", date = " + getDate() + " }";
		}
	}

}
