package ru.vlad805.screenshotsorter.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import ru.vlad805.screenshotsorter.R;

/**
 * vlad805 (c) 2018
 */
public class UpdateBadgeService extends IntentService {

	private static final String CHANNEL_ID = "badge0";
	private static final int NOTIFICATION_ID = 0x1459;

	public static final String EXTRA_COUNT = "count";

	private NotificationManager mNotificationManager;

	public UpdateBadgeService() {
		super("UpdateBadgeService");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	}

	@Override
	public void onHandleIntent(Intent intent) {
		if (intent == null) {
			return;
		}

		int count = intent.getIntExtra(EXTRA_COUNT, 0);

		mNotificationManager.cancel(NOTIFICATION_ID);

		if (Build.MANUFACTURER.equalsIgnoreCase("Xiaomi")) {

			NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
					.setContentTitle("")
					.setContentText("")
					.setSmallIcon(R.drawable.ic_logo_small);

			assert mNotificationManager != null;

			Notification notification = builder.build();

			me.leolin.shortcutbadger.ShortcutBadger.applyNotification(this, notification, count);

			mNotificationManager.notify(NOTIFICATION_ID, notification);
		} else {
			me.leolin.shortcutbadger.ShortcutBadger.applyCount(this, count);
		}
	}

}
